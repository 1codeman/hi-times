/* 
	author: istockphp.com
*/

jQuery(function($) {
	var validation_holder;
	
	$("form#register_form input[name='submit']").click(function() {


	
	var validation_holder = 0;
	
		var fname 			= $("form#register_form input[name='fname']").val();
		var mname			= $("form#register_form input[name='mname']").val();
		var lname 			= $("form#register_form input[name='lname']").val();
		var username 		= $("form#register_form input[name='username']").val();
		var password 		= $("form#register_form input[name='password']").val();
		var repassword 		= $("form#register_form input[name='repassword']").val();
		
		/* validation start */	
		if(fname == "") {
			$("span.val_fname").html("This field is required.").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_fname").html("");
			}
		if(lname == "") {
			$("span.val_lname").html("This field is required.").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_lname").html("");
			}


		if(mname == "") {
			$("span.val_mname").html("This field is required.").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_mname").html("");
			}
		if(username == "") {
			$("span.val_username").html("This field is required.").addClass('validate');
			validation_holder = 1;
		} else {
			$("span.val_username").html("");
			}
		if(password == "") {
			$("span.val_pass").html("This field is required.").addClass('validate');
			validation_holder = 1;
		} else {
				$("span.val_pass").html("");
			}
		if(repassword == "") {
			$("span.val_pass2").html("This field is required.").addClass('validate');
			validation_holder = 1;
		} else {
			if(password != repassword) {
				$("span.val_pass2").html("Password does not match!").addClass('validate');
				validation_holder = 1;
			} else {
				$("span.val_pass2").html("");
			}
		}
		
		if(validation_holder == 1) { // if have a field is blank, return false
			$("p.validate_msg").slideDown("fast");
			return false;
		}  validation_holder = 0; // else return true


		/* validation end */	
	}); // click end 

}); // jQuery End