
<?php
    if (isset($_GET['status'])) {
        if ($_GET['status'] == 1) {
            $msg = '<p class="success">Successfully Registered  <a href="../index.php">Login</a></p>';
        } else {
            $msg = '<p class="failed">Failed to Add Entry</p>';
        }

    } else {
        $msg ="";
    }
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Registrtion</title>
<link href="style/style.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/jquery-1.9.1.js"> </script>
<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="wrap"> <!--wrap start-->
    	<div id="wrap2">  <!--wrap2 start-->
       
       	<h2 class="free_account">Create Account</h2> 
        
    	<form action="process.php" method="post" id="register_form">
        	       
             
                <?php echo $msg ?>
                
                <p class="validate_msg">Please fix the errors below!</p>                
                <p> <label for="name">First Name</label> <input name="fname" type="text" /> <span class="val_fname"></span> </p> 
                <p> <label for="mname">Middle Name</label> <input name="mname" type="text" /> <span class="val_mname"></span> </p>
                <p> <label for="lname">Last Name</label>  <input name="lname" type="text" />  <span class="val_lname"></span> </p>
                <p> <label for="username">Username</label>  <input name="username" type="text" />  <span class="val_username"></span> </p>
                
                <p> <label for="password">Password</label>  <input name="password" type="password" /> <span class="val_pass"></span> </p>
                <p> <label for="repassword">Retype Password</label>  <input name="repassword" type="password" /> <span class="val_pass2"></span> </p>
                
            <input type="submit" name="submit" value="Register">
            
              <a href="../index.php">Back to Login</a>
        </form>

    
       
        </div>  <!--wrap2 end-->
    </div>  <!--wrap start-->
</body>
</html>