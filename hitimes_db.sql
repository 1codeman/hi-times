-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2013 at 12:47 PM
-- Server version: 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hitimes_db`
--
CREATE DATABASE IF NOT EXISTS `hitimes_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hitimes_db`;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `logentryID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `loginTime` datetime DEFAULT NULL,
  `logoutTme` datetime DEFAULT NULL,
  PRIMARY KEY (`logentryID`),
  KEY `logs_ibfk_1` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`logentryID`, `userID`, `loginTime`, `logoutTme`) VALUES
(51, 1, '2013-10-23 01:39:18', '2013-10-23 01:53:03'),
(52, 31, '2013-10-23 01:59:25', '2013-10-23 02:05:46'),
(53, 31, '2013-10-23 02:10:28', '2013-10-23 02:13:19'),
(54, 31, '2013-10-23 02:18:04', '2013-10-23 02:33:42'),
(55, 31, '2013-10-23 02:37:30', '2013-10-23 02:38:50'),
(56, 31, '2013-10-23 02:39:57', '2013-10-23 02:42:18'),
(57, 31, '2013-10-23 02:42:44', '2013-10-23 02:44:28'),
(58, 31, '2013-10-23 02:45:22', '2013-10-23 02:50:26'),
(59, 31, '2013-10-23 02:56:20', '2013-10-23 02:58:44'),
(60, 1, '2013-10-23 03:23:08', NULL),
(61, 35, '2013-10-23 11:37:14', '2013-10-23 13:09:11'),
(62, 26, '2013-10-23 13:09:39', NULL),
(63, 26, '2013-10-23 16:17:07', '2013-10-23 23:50:23'),
(64, 1, '2013-10-23 19:37:16', NULL),
(65, 1, '2013-10-23 19:37:33', NULL),
(66, 1, '2013-10-23 19:37:33', NULL),
(67, 1, '2013-10-23 19:37:35', NULL),
(68, 1, '2013-10-23 19:37:38', NULL),
(69, 1, '2013-10-23 19:37:53', '2013-10-23 19:42:20'),
(70, 1, '2013-10-23 19:51:26', NULL),
(71, 1, '2013-10-23 23:50:47', '2013-10-23 23:51:37'),
(72, 26, '2013-10-23 23:52:10', '2013-10-24 04:07:21'),
(73, 1, '2013-10-24 02:47:41', '2013-10-24 02:48:43'),
(74, 35, '2013-10-24 03:46:21', '2013-10-24 04:11:03'),
(75, 26, '2013-10-24 12:33:05', NULL),
(76, 26, '2013-10-24 12:33:18', NULL),
(77, 26, '2013-10-24 12:33:27', NULL),
(78, 1, '2013-10-24 13:03:14', '2013-10-24 13:04:26'),
(79, 26, '2013-10-24 13:16:38', '2013-10-24 13:17:09'),
(80, 26, '2013-10-24 13:18:08', '2013-10-24 13:28:23'),
(81, 26, '2013-10-24 13:33:31', '2013-10-24 13:33:44'),
(82, 1, '2013-10-24 13:41:47', NULL),
(83, 26, '2013-10-24 13:41:52', NULL),
(84, 1, '2013-10-24 13:41:57', NULL),
(85, 26, '2013-10-24 13:52:49', NULL),
(86, 26, '2013-10-24 13:53:46', '2013-10-24 13:58:06'),
(87, 26, '2013-10-24 14:00:12', '2013-10-24 14:00:32'),
(88, 36, '2013-10-24 14:33:08', '2013-10-24 14:34:11'),
(89, 26, '2013-10-24 14:42:57', '2013-10-24 14:44:30'),
(90, 31, '2013-10-24 14:49:07', '2013-10-24 14:50:03'),
(91, 35, '2013-10-24 14:53:25', '2013-10-24 16:53:01'),
(92, 1, '2013-10-24 17:06:21', '2013-10-24 17:22:03'),
(93, 26, '2013-10-24 18:38:34', '2013-10-24 21:51:19'),
(94, 35, '2013-10-24 21:20:01', NULL),
(95, 35, '2013-10-24 21:20:15', NULL),
(96, 35, '2013-10-24 21:20:21', NULL),
(97, 35, '2013-10-24 21:20:48', '2013-10-25 19:15:48'),
(98, 1, '2013-10-25 19:13:52', NULL),
(99, 1, '2013-10-25 19:14:11', '2013-10-25 19:16:24'),
(100, 31, '2013-10-25 19:17:20', NULL),
(101, 35, '2013-10-25 19:17:44', NULL),
(102, 35, '2013-10-25 19:18:02', '2013-10-25 19:18:44'),
(103, 35, '2013-10-30 18:57:48', '2013-10-30 18:59:14'),
(104, 1, '2013-10-30 19:00:29', '2013-10-30 19:01:13'),
(105, 1, '2013-10-30 19:02:00', '2013-10-30 19:02:29'),
(106, 1, '2013-10-30 19:03:49', '2013-10-30 19:04:07'),
(107, 1, '2013-10-30 19:04:46', '2013-10-30 19:05:06'),
(108, 26, '2013-10-30 19:05:57', '2013-10-30 19:06:14'),
(109, 1, '2013-10-30 19:07:05', '2013-10-30 19:07:51'),
(110, 26, '2013-10-30 19:15:54', '2013-10-30 19:42:12');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `messageDate` datetime NOT NULL,
  `senderID` int(11) NOT NULL,
  `receiverID` int(11) NOT NULL,
  `subject` varchar(30) NOT NULL,
  `message` varchar(300) NOT NULL,
  `isDraft` tinyint(1) NOT NULL,
  PRIMARY KEY (`messageID`),
  KEY `senderID` (`senderID`),
  KEY `receiverID` (`receiverID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=181 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`messageID`, `messageDate`, `senderID`, `receiverID`, `subject`, `message`, `isDraft`) VALUES
(3, '2013-10-21 21:45:08', 1, 26, 'Hello World', 'TESTING!', 0),
(4, '2013-10-21 23:23:47', 26, 1, 'ddsadzdzx', 'zxcxzczccxzczxcc', 0),
(5, '2013-10-22 00:29:07', 26, 1, 'subject', 'hell yeah!', 0),
(9, '2013-10-22 17:28:36', 35, 26, 'hello', 'whats on your mind?', 0),
(10, '2013-10-22 17:29:11', 35, 26, 'hei', 'ano na?', 0),
(23, '2013-10-22 17:52:33', 26, 30, 'sdasdasdasd', 'asdasdsd', 0),
(25, '2013-10-22 18:30:53', 26, 30, 'asdasdasd', 'asdasdasdasdasd', 0),
(26, '2013-10-22 18:44:30', 35, 35, 'hey', 'tryy', 0),
(27, '2013-10-22 18:46:56', 35, 35, 'warning', 'dadasdadad', 0),
(28, '2013-10-22 18:50:38', 35, 1, 'hey doc', 'wahahahahahahha', 0),
(30, '2013-10-22 18:51:44', 35, 1, 'hey', 'call me maybe wahhaha', 0),
(31, '2013-10-22 18:52:56', 35, 26, 'hi five', 'musta?', 0),
(32, '2013-10-22 18:54:13', 35, 31, 'hey', 'shot', 0),
(34, '2013-10-22 18:54:22', 35, 31, 'hey', 'shot', 0),
(35, '2013-10-22 18:55:19', 35, 31, 'hey', 'shot', 0),
(36, '2013-10-22 19:01:59', 26, 27, 'buko', 'asdasfsafasf', 0),
(37, '2013-10-22 19:06:06', 26, 34, 'wooooooo', 'zzzzzzzzzz', 0),
(38, '2013-10-22 19:08:02', 26, 32, 'hoi', 'asdasdasda', 0),
(39, '2013-10-22 19:09:35', 26, 31, 'hey ', 'loco ka', 0),
(40, '2013-10-22 19:11:34', 26, 33, 'strawhat', 'blaaaaaaaaaaaaaaa', 0),
(44, '2013-10-22 21:57:15', 26, 1, 'eqweqw', 'eeeeeeeeeeeeeeeeeeeeee', 0),
(45, '2013-10-22 22:08:49', 26, 29, 'asdasdasd', 'asdasdasdsdasadasd\r\nas\r\ndas\r\nda\r\nsd\r\nassadasddsd', 0),
(49, '2013-10-22 22:55:47', 30, 1, 'Lorem ipsum', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec ', 0),
(50, '2013-10-23 02:12:44', 31, 27, 'assignment', 'no assignment wahaha', 0),
(53, '2013-10-23 13:11:07', 26, 26, 'good', 'asdasd', 0),
(54, '2013-10-23 14:23:03', 26, 1, 'good', 'asdasd', 0),
(85, '2013-10-23 15:10:12', 26, 27, 'dasda', 'asdadasd', 0),
(86, '2013-10-23 15:10:45', 26, 27, 'asdasdasd', 'asdasdasd', 0),
(101, '2013-10-23 15:56:32', 26, 1, 'sadasds', 'asdasdasd', 0),
(118, '2013-10-23 16:29:12', 26, 1, 'sadasds', 'asdasdasd', 0),
(151, '2013-10-23 18:14:04', 26, 27, 'hoi', 'mustasa?', 0),
(153, '2013-10-23 19:20:17', 26, 26, 'RE:asdasdasdsad', 'sdfsdf', 0),
(155, '2013-10-23 19:39:13', 1, 34, 'hello', 'meeeeeeeeeeeeeeeeee', 0),
(156, '2013-10-23 19:40:18', 1, 26, 'RE:sadasds', 'tra?', 0),
(157, '2013-10-23 19:52:35', 1, 26, 'RE:sadasds', 'woosfsddavsdfsd\r\nf\r\nsdf\r\nsd\r\nfsd\r\nfs\r\ndfsdf', 0),
(161, '2013-10-24 01:13:03', 26, 28, 'brap', 'message', 1),
(162, '2013-10-24 01:53:17', 26, 32, 'trydraft', 'sadadadsad', 0),
(163, '2013-10-24 02:14:53', 26, 32, 'trydraft', 'sadadadsad', 0),
(164, '2013-10-24 02:16:28', 26, 32, 'trydraft', 'sadadadsad', 0),
(165, '2013-10-24 02:23:49', 26, 1, 'draft', 'asdsdadasdasdd', 0),
(167, '2013-10-24 02:25:09', 26, 26, 'draftsample', 'asdasdasdasdsd', 0),
(171, '2013-10-24 02:30:02', 26, 26, 'awesome', 'dsdlaksdjlaskdlasd', 0),
(174, '2013-10-24 02:48:30', 1, 28, 'heeeyaa', 'asdasdasd', 1),
(175, '2013-10-24 14:33:54', 36, 26, 'project', 'done :D', 0),
(176, '2013-10-24 14:49:43', 31, 29, 'hey wiz', 'asdasdasdasd', 1),
(177, '2013-10-24 21:21:36', 35, 26, 'heyyyy', 'xdgfdfdgffsdsfsf', 0),
(178, '2013-10-30 19:16:54', 26, 29, 'vvvvvvvvvvvv', 'asdadasd', 0),
(179, '2013-10-30 19:17:56', 26, 1, 'RE:Hello World', 'musta?', 0),
(180, '2013-10-30 19:18:46', 26, 26, 'trydraft', 'adasdasda', 1);

-- --------------------------------------------------------

--
-- Table structure for table `metausers`
--

CREATE TABLE IF NOT EXISTS `metausers` (
  `metaID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `userID_fk` int(11) NOT NULL,
  PRIMARY KEY (`metaID`),
  UNIQUE KEY `username` (`username`),
  KEY `metausers_ibfk_1` (`userID_fk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `metausers`
--

INSERT INTO `metausers` (`metaID`, `username`, `password`, `userID_fk`) VALUES
(1, 'nestabob', 'd8578edf8458ce06fbc5bb76a58c5ca4', 1),
(16, 'francoreyes', 'd8578edf8458ce06fbc5bb76a58c5ca4', 26),
(17, 'tipong', 'd8578edf8458ce06fbc5bb76a58c5ca4', 27),
(18, 'jonnel', 'd8578edf8458ce06fbc5bb76a58c5ca4', 28),
(19, 'wizkhalifa', 'd8578edf8458ce06fbc5bb76a58c5ca4', 29),
(20, 'juandelacruz', 'd8578edf8458ce06fbc5bb76a58c5ca4', 30),
(21, 'mackyloco', 'd8578edf8458ce06fbc5bb76a58c5ca4', 31),
(22, 'airness23', 'd8578edf8458ce06fbc5bb76a58c5ca4', 32),
(23, 'strawhatcaptain', 'd8578edf8458ce06fbc5bb76a58c5ca4', 33),
(24, 'chokola', 'd8578edf8458ce06fbc5bb76a58c5ca4', 34),
(25, 'admin', '21232f297a57a5a743894a0e4a801fc3', 35),
(26, 'vorold', 'd8578edf8458ce06fbc5bb76a58c5ca4', 36);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `fName` varchar(50) NOT NULL,
  `mName` varchar(50) NOT NULL,
  `lName` varchar(50) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `fName`, `mName`, `lName`) VALUES
(1, 'nesta ', 'bob', 'marley'),
(26, 'franco', 'islaw', 'reyes'),
(27, 'jolino', 'buko', 'casidsid'),
(28, 'jonnel', 'abias', 'jones'),
(29, 'wiz', 'devin', 'khalifa'),
(30, 'juan', 'dela', 'cruz'),
(31, 'macky', 'loco', 'belaro'),
(32, 'michael', 'air', 'jordan'),
(33, 'monkey', 'd', 'luffy'),
(34, 'maria', 'chan', 'yap'),
(35, 'delon', 'nesta', 'trinidad'),
(36, 'harold', 'robot', 'candelaria');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`senderID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`receiverID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `metausers`
--
ALTER TABLE `metausers`
  ADD CONSTRAINT `metausers_ibfk_1` FOREIGN KEY (`userID_fk`) REFERENCES `users` (`userID`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
