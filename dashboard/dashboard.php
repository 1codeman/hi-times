
<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>Hi-Times</title>
	<link rel="stylesheet" type="text/css" href="style1.css">

	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.3.4/themes/black/easyui.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.3.4/themes/icon.css">
	
	<style type="text/css">

		body{

			background: #7e7e7e;
		}
	
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			color:#666;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
	</style>
	<script type="text/javascript" src="script/jquery.min.js"></script>
	<script type="text/javascript" src="script/jquery.easyui.min.js"></script>
	<script type="text/javascript">
		var url;
		var urlDraft;
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Write Message');
			$('#fm').form('clear');
			url = 'save_user.php';
			urlDraft = 'save_draft.php';
		}
		function reply(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				row['message'] = "";
				var subject = "RE:";
				var temp = row['subject'];
				subject +=row['subject'];
				row['subject'] = subject;
				$('#dlgVIEW').dialog('close');
				$('#dlgREPLY').dialog('open').dialog('setTitle','Reply');
				$('#fmREPLY').form('load',row);
				row['subject']=temp;
				url = 'reply_message.php?id=' +row.userID;
			}
		}function closeReply(){
			$('#dg').datagrid('reload');
			$('#dlgREPLY').dialog('close');

		}

		function saveDraft(){
			$('#fm').form('submit',{
				url: urlDraft,
				onSubmit: function(){
					return $(this).form('validate');
				},

				success: function(result){
					var result = eval('('+result+')');
					if (result.success){

						$('#dlg').dialog('close');		// close the dialog
						$('#dgDRAFT').datagrid('reload');// reload the user data
						$.messager.show({
							title: 'Ayos!',
							msg: 'Save to Draft!'
						});	
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});

		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlgVIEW').dialog('open').dialog('setTitle','Message');
				$('#fmVIEW').form('load',row);
				url = 'update_user.php?id='+row.userID;
			}
		}

		function viewDraft(){
			var row = $('#dgDRAFT').datagrid('getSelected');
			if (row){
				$('#dlgDRAFT').dialog('open').dialog('setTitle','Draft');
				$('#fmDRAFT').form('load',row);
				url = 'send_draft.php?receiver='+row.userID;
			}
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},

				success: function(result){
					var result = eval('('+result+')');
					if (result.success){

						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');// reload the user data
						$.messager.show({
							title: 'Ayos!',
							msg: 'Message Sent Amigo!'
						});	
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});
		}
		function sendDraft(){
			$('#fmDRAFT').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},

				success: function(result){
					var result = eval('('+result+')');
					if (result.success){

						removeDraft();

						$('#dlgDRAFT').dialog('close');
						$('#dg').datagrid('reload');		
						$('#dgDRAFT').datagrid('reload');
			
					
						$.messager.show({
							title: 'Ayos!',
							msg: 'Message Sent Amigo!'
						});	
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});
		}

		function sendReply(){
			$('#fmREPLY').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},

				success: function(result){
					var result = eval('('+result+')');
					if (result.success){

						$('#dlgREPLY').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');// reload the user data
						$.messager.show({
							title: 'Ayos!',
							msg: 'Message Sent Amigo!'
						});	
					} else {
						$.messager.show({
							title: 'Error',
							msg: result.msg
						});
					}
				}
			});
		}
		function removeDraft(){
			var row = $('#dgDRAFT').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to delete this message?',function(r){
					if (r){
						$.post('remove_user.php',{id:row.messageID},function(result){
							if (result.success){
								$('#dgDRAFT').datagrid('reload');// reload the user data
								$.messager.show({
							title: 'Uhhhrggg!',
							msg: 'Message Deleted!'
							});		
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.msg
								});
							}
						},'json');
					}
				});
			}
		}
		function removeUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to delete this message?',function(r){
					if (r){
						$.post('remove_user.php',{id:row.messageID},function(result){
							if (result.success){
								$('#dg').datagrid('reload');// reload the user data
								$.messager.show({
							title: 'Uhhhrggg!',
							msg: 'Message Deleted!'
							});		
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.msg
								});
							}
						},'json');
					}
				});
			}
		}
	</script>
</head>
<body>
	<center>
		<?php 
		
		json_encode(array('msg'=>'Message Sent. Hi Five :D'));
		if(isset($_SESSION['username'])||isset($_COOKIE['hightimes_db'])){

		
		include('conn.php');
		

		}else{

			echo "Access denied!";

		}

		?>
	<br>
	<!-- TAB -->

	
	<div class="demo-info" style="width:700px;height:15px;background:#383737;color:#0fd591">
		<div class="demo-tip " ></div>
		<div><?php include('../session.php');?></div>
	</div>
	<div style="margin:10px 0;"></div>
	<div class="easyui-tabs" style="width:720px;height:310px;">
		<div title="Welcome" style="padding:10px">
			<p style="font-size:14px"><center>MINI MESSAGING WEB APPLICATION</center></p>
	
		</div>

		<div title="Inbox" style="padding:10px">
			
	<table id="dg" title="" class="easyui-datagrid" style="width:700px;height:250px"
			url="get_users.php"
			toolbar="#toolbar" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="fName" width="50">From</th>
				<th field="subject" width="50">Subject</th>
				<th field="messageDate" width="50">Date</th>
			
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">Compose</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">View Message</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="removeUser()">Delete Message</a>
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle"></div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label>To:</label>
				<select class="easyui-combobox" name="receiver" required="true" style = "width:170px;">


					<?php
						include('conn.php');
    					$query = "SELECT * FROM `users`";
    					$result = mysql_query($query);
    					while($row=mysql_fetch_array($result, MYSQL_ASSOC)){                                                 
       						echo "<option value='".$row['userID']."'>".$row['fName']."</option>";
    				}
?>

				</select>
			</div>
			<div class="fitem">
				<label>Subject:</label>
				<input name="subject" class="easyui-validatebox" required="true " style = "width:170px;">
			</div>
			<div class="fitem">
				<label>Message:</label>
				<textarea name="message" class="easyui-validatebox" required="true "  style="height:80px; width: 170px;"></textarea>
			</div>
		
		</form>
	</div>
	<div id="dlg-buttons">

		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">Send</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="saveDraft()">Save to Draft</a>
	</div>

	<!--VIEW MESSAGE-->


	<div id="dlgVIEW" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
			closed="true" buttons="#dlgVIEW-buttons">
		<div class="ftitle">Message View</div>
		<form id="fmVIEW" method="post" novalidate>
			<div class="fitem">
				<label>From:</label>
				<select disabled ="disabled" class="easyui-combobox" name="fName" required="true" style = "width:170px; ">


					<?php
						include('conn.php');
    					$query = "SELECT * FROM `users`";
    					$result = mysql_query($query);
    					while($row=mysql_fetch_array($result, MYSQL_ASSOC)){                                                 
       						echo "<option disabled value='".$row['userID']."'>".$row['fName']."</option>";
    				}
			?>

				</select>
			</div>
			<div class="fitem">
				<label>Subject:</label>
				<input readonly name="subject" class="easyui-validatebox" required="true " style = "width:170px;">
			</div>
			<div class="fitem">
				<label>Message:</label>
				<textarea readonly name="message" class="easyui-validatebox" required="true "  style="height:80px; width: 170px;"></textarea>
			</div>
		
		</form>
	</div>
	<div id="dlgVIEW-buttons">

		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="reply()">Reply</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgVIEW').dialog('close')">Cancel</a>
	</div>



	<!--END VIEW MESSAGE-->


			<!--REPLY MESSAGE-->

			<div id="dlgREPLY" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
			closed="true" buttons="#dlgREPLY-buttons">
		<div class="ftitle">Reply</div>
		<form id="fmREPLY" method="post" novalidate>
			<div class="fitem">
				<label>To:</label>
				<select   disabled = "disabled" class="easyui-combobox" name="fName" required="true" style = "width:170px; ">

				</select>
			</div>
			<div class="fitem">
				<label>Subject:</label>
				<input name="subject" class="easyui-validatebox" required="true " style = "width:170px;">
			</div>
			<div class="fitem">
				<label>Message:</label>
				<textarea  name="message" class="easyui-validatebox" required="true "  style="height:80px; width: 170px;"> </textarea>
			</div>
		
		</form>
	</div>
	<div id="dlgREPLY-buttons">

		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="sendReply()">Send</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="closeReply()">Cancel</a>
	</div>

		<!--END REPLY MESSAGE-->

		</div>
		<div title="Draft" style="padding:10px">
			<table id="dgDRAFT" title="" class="easyui-datagrid" style="width:700px;height:250px"
			url="get_drafts.php"
			toolbar="#toolbarDraft" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="fName" width="50">To</th>
				<th field="subject" width="50">Subject</th>
			
			</tr>
		</thead>
	</table>
	<div id="toolbarDraft">
		
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="viewDraft()">View Message</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="removeDraft()">Delete Message</a>
	</div>
	
	<div id="dlgDRAFT" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
			closed="true" buttons="#dlgDRAFT-buttons">
		<div class="ftitle"></div>
		<form id="fmDRAFT" method="post" novalidate>
			<div class="fitem">
				<label>To:</label>
				<select class="easyui-combobox" name="fName" required="true" style = "width:170px;">


					<?php
						include('conn.php');
    					$query = "SELECT * FROM `users`";
    					$result = mysql_query($query);
    					while($row=mysql_fetch_array($result, MYSQL_ASSOC)){                                                 
       						echo "<option value='".$row['userID']."'>".$row['fName']."</option>";
    				}
?>

				</select>
			</div>
			<div class="fitem">
				<label>Subject:</label>
				<input name="subject" class="easyui-validatebox" required="true " style = "width:170px;">
			</div>
			<div class="fitem">
				<label>Message:</label>
				<textarea name="message" class="easyui-validatebox" required="true "  style="height:80px; width: 170px;"></textarea>
			</div>
		
		</form>
	</div>
	<div id="dlgDRAFT-buttons">

		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="sendDraft()">Send</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgDRAFT').dialog('close')">Cancel</a>
	</div>

	<!--VIEW MESSAGE-->


	<div id="dlgVIEW" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
			closed="true" buttons="#dlgVIEW-buttons">
		<div class="ftitle">Message View</div>
		<form id="fmVIEW" method="post" novalidate>
			<div class="fitem">
				<label>From:</label>
				<select disabled ="disabled" class="easyui-combobox" name="fName" required="true" style = "width:170px; ">


					<?php
						include('conn.php');
    					$query = "SELECT * FROM `users`";
    					$result = mysql_query($query);
    					while($row=mysql_fetch_array($result, MYSQL_ASSOC)){                                                 
       						echo "<option disabled value='".$row['userID']."'>".$row['fName']."</option>";
    				}
			?>

				</select>
			</div>
			<div class="fitem">
				<label>Subject:</label>
				<input readonly name="subject" class="easyui-validatebox" required="true " style = "width:170px;">
			</div>
			<div class="fitem">
				<label>Message:</label>
				<textarea readonly name="message" class="easyui-validatebox" required="true "  style="height:80px; width: 170px;"></textarea>
			</div>
		
		</form>
	</div>
	<div id="dlgVIEW-buttons">

		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="reply()">Reply</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgVIEW').dialog('close')">Cancel</a>
	</div>



	<!--END VIEW MESSAGE-->


			<!--REPLY MESSAGE-->

			<div id="dlgREPLY" class="easyui-dialog" style="width:400px;height:280px;padding:10px 20px"
			closed="true" buttons="#dlgREPLY-buttons">
		<div class="ftitle">Reply</div>
		<form id="fmREPLY" method="post" novalidate>
			<div class="fitem">
				<label>To:</label>
				<select   disabled = "disabled" class="easyui-combobox" name="fName" required="true" style = "width:170px; ">

				</select>
			</div>
			<div class="fitem">
				<label>Subject:</label>
				<input name="subject" class="easyui-validatebox" required="true " style = "width:170px;">
			</div>
			<div class="fitem">
				<label>Message:</label>
				<textarea  name="message" class="easyui-validatebox" required="true "  style="height:80px; width: 170px;"> </textarea>
			</div>
		
		</form>
	</div>
	<div id="dlgREPLY-buttons">

		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="sendReply()">Send</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="closeReply()">Cancel</a>
	</div>
	
		</div>
		<div title="Help" data-options="iconCls:'icon-help',closable:true" style="padding:10px">
			<p align="justify">
Lorem ipsum dolor sit amet, scelerisque quam sit nunc urna nulla. Imperdiet magna urna. Ante pede morbi donec ipsum, eros vivamus augue, diam dui pede, interdum ultricies phasellus placerat, sit a placerat adipiscing. Pede vitae magna pretium volutpat, auctor wisi praesent mauris amet nonummy, ligula diam vitae amet. Nam suscipit elit faucibus, volutpat natoque eget tempor integer, vulputate aliquam magna gravida amet lectus cursus, imperdiet sit eleifend sit eros massa, massa nulla id aliquam vestibulum mattis.</p>
<p align="justify">
</p>
		</div>
	</div>

		<!-- END TAB -->
<div>
<div class="demo-info" style="width:696px;height:15px;background:#383737;color:#0fd591">
		<div class="demo-tip " ></div>
		<div>(c)Delon.Trinidad CS</div>
	</div>
</div>


</body>
</html>